package codes.delia.recents.model;

import android.view.View;

import codes.delia.recents.R;

/**
 * Created by deliafernandez on 15-02-22.
 */
public enum RecentType {
    RECENT_1(0, R.string.recent_1,R.color.recent_color_1, View.NO_ID, R.drawable.task_icon_dark),
    RECENT_1_T(1,R.string.recent_1,R.color.recent_color_1,R.drawable.recents_thumbnail, R.drawable.task_icon_dark),
    RECENT_2(2,R.string.recent_2,R.color.recent_color_2,View.NO_ID, R.drawable.task_icon),
    RECENT_2_T(3,R.string.recent_2,R.color.recent_color_2,R.drawable.recents_thumbnail_2, R.drawable.task_icon),
    RECENT_3(4,R.string.recent_3,R.color.recent_color_3,View.NO_ID, R.drawable.task_icon),
    RECENT_3_T(5,R.string.recent_3,R.color.recent_color_3,R.drawable.recents_thumbnail_3, R.drawable.task_icon);

    public final int rTitleId;
    public final int rColorId;
    public final int position;
    public final int rThumbnailId;
    public final int rTaskIconId;

    RecentType(int position, int rTitleId,int rColorId, int thumbnail,int taskIcon){
        this.position = position;
        this.rTitleId = rTitleId;
        this.rColorId = rColorId;
        this.rThumbnailId = thumbnail;
        this.rTaskIconId = taskIcon;
    }

    public static RecentType getTypeByPosition(int position) {
        for(RecentType recentType:values()) {
            if(recentType.position == position){
                return recentType;
            }
        }
        return RECENT_1;
    }
}

