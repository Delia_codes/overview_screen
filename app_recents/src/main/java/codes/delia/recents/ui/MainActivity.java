package codes.delia.recents.ui;

import android.app.ActivityManager;
import android.content.Intent;
import android.graphics.BitmapFactory;
import android.os.Bundle;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBar;
import android.support.v7.app.ActionBarActivity;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.TextView;

import codes.delia.recents.R;
import codes.delia.recents.model.RecentType;
import codes.delia.recents.ui.fragment.NavigationDrawerFragment;


public class MainActivity extends ActionBarActivity
        implements NavigationDrawerFragment.NavigationDrawerCallbacks {

    private static final String EXTRA_RECENT_OPTION = "extra_recent_option";
    /**
     * Fragment managing the behaviors, interactions and presentation of the navigation drawer.
     */
    private NavigationDrawerFragment mNavigationDrawerFragment;

    /**
     * Used to store the last screen title. For use in {@link #restoreActionBar()}.
     */
    private CharSequence mTitle;

    private RecentType recentType = RecentType.RECENT_1;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        mNavigationDrawerFragment = (NavigationDrawerFragment)
                getSupportFragmentManager().findFragmentById(R.id.navigation_drawer);
        mTitle = getTitle();

        // Set up the drawer.
        mNavigationDrawerFragment.setUp(
                R.id.navigation_drawer,
                (DrawerLayout) findViewById(R.id.drawer_layout));

        if(getIntent() != null && getIntent().hasExtra(EXTRA_RECENT_OPTION)) {
            recentType = RecentType.valueOf(getIntent().getStringExtra(EXTRA_RECENT_OPTION));

            //If the activity is instantiated as a new recent section it will modify task's overview properties
        } else {
            recentType = RecentType.RECENT_1;
        }
        TextView content = (TextView) findViewById(R.id.content);
        View container = findViewById(R.id.container);
        content.setText(recentType.rTitleId);
        container.setBackgroundColor(getResources().getColor(recentType.rColorId));
    }

    @Override
    public void onNavigationDrawerItemSelected(int position) {
        navigateTo(position);
    }

    @Override
    protected void onResume() {
        super.onResume();
        if(recentType != null && recentType.rThumbnailId == View.NO_ID) {
            ActivityManager.TaskDescription taskDescription = new ActivityManager.TaskDescription(getString(recentType.rTitleId),
                    BitmapFactory.decodeResource(getResources(),recentType.rTaskIconId),
                    getResources().getColor(recentType.rColorId));
            setTaskDescription(taskDescription);
        }
    }

    public void navigateTo(int position) {
        Intent intent = new Intent(this, MainActivity.class);
        RecentType selectedRecent = RecentType.getTypeByPosition(position);
        if (selectedRecent != recentType) {
            intent.putExtra(EXTRA_RECENT_OPTION, selectedRecent.name());
            intent.addCategory(Intent.CATEGORY_LAUNCHER);
            intent.addFlags(Intent.FLAG_ACTIVITY_NEW_DOCUMENT);
            if(selectedRecent.rThumbnailId != View.NO_ID) {
                ActivityManager activityManager = (ActivityManager) getSystemService(ACTIVITY_SERVICE);
                ActivityManager.TaskDescription taskDescription = new ActivityManager
                        .TaskDescription(getString(selectedRecent.rTitleId),
                        BitmapFactory.decodeResource(getResources(),selectedRecent.rTaskIconId),
                        getResources().getColor(selectedRecent.rColorId));
                int taskId = activityManager.addAppTask(this,
                        getIntent(),
                        taskDescription,
                        BitmapFactory.decodeResource(getResources(), selectedRecent.rThumbnailId));
                if(taskId != View.NO_ID) {
                    activityManager.moveTaskToFront(taskId, ActivityManager.MOVE_TASK_NO_USER_ACTION, null);
                }

            } else {
                intent.addFlags(Intent.FLAG_ACTIVITY_MULTIPLE_TASK);
            }
            startActivity(intent);
        }
    }
    
    public void restoreActionBar() {
        ActionBar actionBar = getSupportActionBar();
        actionBar.setNavigationMode(ActionBar.NAVIGATION_MODE_STANDARD);
        actionBar.setDisplayShowTitleEnabled(true);
        actionBar.setTitle(mTitle);
    }


    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        if (!mNavigationDrawerFragment.isDrawerOpen()) {
            // Only show items in the action bar relevant to this screen
            // if the drawer is not showing. Otherwise, let the drawer
            // decide what to show in the action bar.
            getMenuInflater().inflate(R.menu.main, menu);
            restoreActionBar();
            return true;
        }
        return super.onCreateOptionsMenu(menu);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }
}
